package com.rss.project.kalkulator;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class Podstawowy extends Activity {

    public String str ="";
    public String temp[];
    Character op = 'q';
    double i,num,numtemp;
    EditText showResult;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podstawowy);

        showResult = (EditText)findViewById(R.id.result_id);
        Toast.makeText(getApplicationContext(), "kalkulator przyjmuje tylko liczby", Toast.LENGTH_LONG).show();


    }
    public void btn1Clicked(View data){
        insert(1);

    }

    public void btn2Clicked(View data){
        insert(2);

    }
    public void btn3Clicked(View data){
        insert(3);

    }
    public void btn4Clicked(View data){
        insert(4);

    }
    public void btn5Clicked(View data){
        insert(5);

    }
    public void btn6Clicked(View data){
        insert(6);
    }
    public void btn7Clicked(View data){
        insert(7);

    }
    public void btn8Clicked(View data){
        insert(8);

    }
    public void btn9Clicked(View data){
        insert(9);

    }
    public void btnzeroClicked(View data){
        insert(0);

    }
    public void btnplusClicked(View data){
        perform();
        op = '+';

    }

    public void btnminusClicked(View data){
        perform();
        op = '-';

    }
    public void btndivideClicked(View data){
        perform();
        op = '/';

    }
    public void btnmultiClicked(View data){
        perform();
        op = '*';

    }
    public void btnequalClicked(View data){
        calculate();

    }

    public void btnclearClicked(View data){
        reset();
    }

    public void btnznakclicked(View data){

        if (showResult.getText().equals('-')){
            int temp = showResult.getText().toString().length();
            temp-=1;
            showResult.setText(showResult.getText().toString().substring(0, (showResult.getText().toString().length() - temp )));
            //showResult.setText(showResult.getText().toString().substring(1, (showResult.getText().toString().length() - 1)));
        }
        else showResult.setText("-" +showResult.getText());

    }
    public void openAdvanced(View view) {
        Intent intent = new Intent(this, zaawansowany.class);
        startActivity(intent);
    }


    public void btnbckClicked(View data){
        if(showResult.getText().length() != 0)
        {
            showResult.setText(showResult.getText().toString().substring(0, (showResult.getText().toString().length() - 1) ));
        }
    }
    public void btnznakClicked(View data){

        if (showResult.getText().equals('-')){
            int temp = showResult.getText().toString().length();
            temp-=1;
            showResult.setText(showResult.getText().toString().substring(0, (showResult.getText().toString().length() - temp )));
            //showResult.setText(showResult.getText().toString().substring(1, (showResult.getText().toString().length() - 1)));
        }
        else showResult.setText("-" +showResult.getText());



    }
    private void reset() {
        // TODO Auto-generated method stub
        str ="";
        op ='q';
        num = 0;
        numtemp = 0;
        showResult.setText("");
    }
    private void insert(int j) {
        // TODO Auto-generated method stub
        str = str+Integer.toString(j);
        if (showResult.equals("[a-zA-Z]+")){
            Toast.makeText(getApplicationContext(), "nieprawidłowe dane!!!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            num = Integer.valueOf(str).intValue();
            showResult.setText(str);
        }


    }
    private void perform() {
        // TODO Auto-generated method stub
        str = "";
        numtemp = num;
    }


    public void iloraz(double a, double b) {
        if (b != 0) {
            num = a / b;
        } else {
            Toast.makeText(getApplicationContext(), "Nie można dzielić przez 0!!!", Toast.LENGTH_SHORT).show();
            showResult.setText(showResult.getText().toString().substring(0, (showResult.getText().toString().length() - 1) ));
            showResult.setText("");
        }
    }
    private void calculate() {
        // TODO Auto-generated method stub
        //try{
        if(op == '+')
            num = numtemp+num;
        else if(op == '-')
            num = numtemp-num;
        else if(op == '/')
            iloraz(numtemp,num);
        else if(op == '*')
            num = numtemp*num;
        showResult.setText(""+num);
        // }catch(ArithmeticException  e)
        // {
        //	Toast.makeText(getApplicationContext(), "Nie można dzielić przez 0!!!", Toast.LENGTH_SHORT).show();
        //	 showResult.setText(showResult.getText().toString().substring(0, (showResult.getText().toString().length() - 1) ));
        // }
    }

}